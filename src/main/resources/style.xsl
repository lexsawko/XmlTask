<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:import-schema namespace="schema.xsd" schema-location="schema.xsd" />

    <xsl:template match="/">
        <html>
            <head><title>XML Orders</title></head>
            <body><h2>Order list:</h2>
                <xsl:for-each select="order_list/order">
                    <hr/>
                    <h3>Order:</h3>

                    <table border="1">
                        <tr bgcolor="#9acd32">
                            <td>-/-</td>
                            <td>Country</td>
                            <td>First name</td>
                            <td>Last name</td>
                            <td>Street</td>
                            <td>City</td>
                            <td>State</td>
                            <td>Zip</td>
                        </tr>
                        <tr>
                            <td>Shipping to</td>
                            <td><xsl:value-of select="shipping_to/@country" /></td>
                            <td><xsl:value-of select="shipping_to/first_name" /></td>
                            <td><xsl:value-of select="shipping_to/last_name" /></td>
                            <td><xsl:value-of select="shipping_to/street" /></td>
                            <td><xsl:value-of select="shipping_to/city" /></td>
                            <td><xsl:value-of select="shipping_to/state" /></td>
                            <td><xsl:value-of select="shipping_to/zip" /></td>
                        </tr>
                        <tr>
                            <td>Bill to</td>
                            <td><xsl:value-of select="bill_to/@country" /></td>
                            <td><xsl:value-of select="bill_to/first_name" /></td>
                            <td><xsl:value-of select="bill_to/last_name" /></td>
                            <td><xsl:value-of select="bill_to/street" /></td>
                            <td><xsl:value-of select="bill_to/city" /></td>
                            <td><xsl:value-of select="bill_to/state" /></td>
                            <td><xsl:value-of select="bill_to/zip" /></td>
                        </tr>
                    </table>
                    <h3>Item list:</h3>
                    <table border="1">
                        <tr bgcolor="#9acd32">
                            <td>Code</td>
                            <td>Item name</td>
                            <td>Quantity</td>
                            <td>Price</td>
                            <td>Order date</td>
                        </tr>
                        <xsl:for-each select="item_list/item">
                            <tr>
                                <td><xsl:value-of select="@code" /></td>
                                <td><xsl:value-of select="item_name" /></td>
                                <td><xsl:value-of select="quantity" /></td>
                                <td><xsl:value-of select="price" /></td>
                                <td><xsl:value-of select="order_date" /></td>
                            </tr>
                        </xsl:for-each>
                    </table>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>

