package DomParser;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class DocumentHandler {
    private Document document;

    public DocumentHandler(Document document) {
        this.document = document;
    }

    public void handleDocument() {
        Element root = document.getDocumentElement();
        System.out.println("Orders:");
        NodeList nodeList = root.getElementsByTagName("order");
        System.out.println("------------------------");

        for (int index = 0; index < nodeList.getLength(); index++) {
            System.out.println("Order " +(index+1) + ":\n");
            Element orderElement = (Element) nodeList.item(index);
            printOrder(orderElement);
            System.out.println("------------------------");
        }
        System.out.println("DOM парсер закончил работу\n");
    }

    private void printOrder(Element orderElement) {
        Element shippingTo = (Element) orderElement.getElementsByTagName("shipping_to").item(0);
        printShippingOrBill(shippingTo, true);
        Element billTo = (Element) orderElement.getElementsByTagName("bill_to").item(0);
        printShippingOrBill(billTo, false);
        Element itemList = (Element) orderElement.getElementsByTagName("item_list").item(0);
        printItemList(itemList);
    }

    private void printShippingOrBill(Element shipping, boolean isShipping) {
        if (isShipping) {
            System.out.println("Отправка в " + shipping.getAttribute("country"));
        } else {
            System.out.println("Плательщик из станы " + shipping.getAttribute("country"));
        }
        System.out.println("Имя: " + shipping.getElementsByTagName("first_name").item(0).getTextContent());
        System.out.println("Фамилия: " + shipping.getElementsByTagName("last_name").item(0).getTextContent());
        System.out.println("Адрес: " + shipping.getElementsByTagName("street").item(0).getTextContent());
        if ((shipping.getElementsByTagName("state").item(0)) != null) {
            System.out.println("Область: " + shipping.getElementsByTagName("state").item(0).getTextContent());
        }
        System.out.println("Индекс: " + shipping.getElementsByTagName("zip").item(0).getTextContent() + "\n");
    }

    private void printItemList(Element itemList) {
        NodeList items = itemList.getElementsByTagName("item");
        System.out.println("Товары:");
        for (int index = 0; index < items.getLength(); index++) {
            Element item = (Element) items.item(index);
            System.out.println("Код товара: " + item.getAttribute("code"));
            System.out.println("Наименование: " + item.getElementsByTagName("item_name").item(0).getTextContent());
            System.out.println("Количество: " + item.getElementsByTagName("quantity").item(0).getTextContent());
            System.out.println("Цена: $" + item.getElementsByTagName("price").item(0).getTextContent());
            System.out.println("Дата заказа: " + item.getElementsByTagName("order_date").item(0).getTextContent() + "\n");
        }
    }
}
