import DomParser.DocumentHandler;
import DomParser.DomBuilder;
import SaxParser.OrderHandler;
import SaxParser.OrderParser;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class Runner {
    public static void main(String[] args) {
        DomBuilder domBuilder = new DomBuilder("order_list.xml","schema.xsd");
        DocumentHandler documentHandler = new DocumentHandler(domBuilder.buildDocument());
        documentHandler.handleDocument();
        OrderParser orderParser = new OrderParser();
        orderParser.parse("order_list.xml",new OrderHandler());
    }
}
