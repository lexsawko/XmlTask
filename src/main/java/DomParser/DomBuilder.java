package DomParser;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class DomBuilder {
    private String xmlFileName;
    private String schemaFileName;
    private DocumentBuilder builder;
    private Document document;
    private final String LANGUAGE = XMLConstants.W3C_XML_SCHEMA_NS_URI;

    public DomBuilder(String xmlFileName, String schemaFileName) {
        this.xmlFileName = xmlFileName;
        this.schemaFileName = schemaFileName;
        System.out.println("DOM парсер начал работу");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            System.err.println("Ошибка конфигурации парсера: " + e);
        }
    }

    public Document buildDocument() {
        document = null;
        try {
            document = builder.parse(getFile(xmlFileName));
        } catch (SAXException | IOException e) {
            System.err.println("Ошибка парсинга файла: " + e);
        }
        document.getDocumentElement().normalize();
        validateXML(document);
        return document;
    }

    private void validateXML(Document document) {
        Schema schema = null;
        try {
            SchemaFactory schemaFactory = SchemaFactory.newInstance(LANGUAGE);
            schema = schemaFactory.newSchema(getFile(schemaFileName));
            Validator validator = schema.newValidator();
            validator.validate(new DOMSource(document));
        } catch (IOException | SAXException e) {
            System.err.println("Ошибка валидации: " + e);
        }
    }

    private File getFile(String fileName){
        ClassLoader classLoader = getClass().getClassLoader();
        return new File(classLoader.getResource(fileName).getFile());
    }


}
