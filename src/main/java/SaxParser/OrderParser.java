package SaxParser;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;

public class OrderParser {
    public void parse(String fileName, OrderHandler orderHandler){
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(getFile(fileName),orderHandler);
        } catch (ParserConfigurationException e) {
            System.err.println("ParserConfigurationException" + e);
        } catch (SAXException e) {
            System.err.println("SAXException" + e);
        } catch (IOException e) {
            System.err.println("IOException" + e);
        }
    }

    private File getFile(String fileName){
        ClassLoader classLoader = getClass().getClassLoader();
        return new File(classLoader.getResource(fileName).getFile());
    }
}
