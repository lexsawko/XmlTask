package SaxParser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order {
    private ShippingAndBillTo shippingTo;
    private ShippingAndBillTo billTo;
    private List<Item> itemList;

    public Order(){
        shippingTo = new ShippingAndBillTo();
        billTo = new ShippingAndBillTo();
        itemList = new ArrayList<>();
    }

    public ShippingAndBillTo getShippingTo() {
        return shippingTo;
    }

    public ShippingAndBillTo getBillTo() {
        return billTo;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    @Override
    public String toString() {
        String items="";
        for (Item item:itemList) {
            items+=item.toString();
        }
        return "\nOrder:\n" +
                "ShippingTo: \n" + shippingTo +
                "BillTo: \n" + billTo +
                "ItemList: \n" + items;
    }

    public class ShippingAndBillTo{
        private String country;
        private String firstName;
        private String lastName;
        private String street;
        private String city;
        private String state;
        private Integer zip;

        public void setCountry(String country) {
            this.country = country;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public void setState(String state) {
            this.state = state;
        }

        public void setZip(int zip) {
            this.zip = zip;
        }

        @Override
        public String toString() {
            return  "\tCountry=: " + country +
                    "\n\tFirstName: " + firstName +
                    "\n\tLastName: " + lastName +
                    "\n\tStreet: " + street +
                    "\n\tCity: " + city +
                    "\n\tState: " + state +
                    "\n\tZip: " + zip + "\n";
        }
    }

    public class Item{
        private Integer code;
        private String itemName;
        private Integer quantity;
        private Float price;
        private Date orderDate;

        public void setCode(Integer code) {
            this.code = code;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public void setPrice(Float price) {
            this.price = price;
        }

        public void setOrderDate(Date orderDate) {
            this.orderDate = orderDate;
        }

        @Override
        public String toString() {
            return "Item: \n" +
                    "\tCode: " + code +
                    "\n\tItemName: " + itemName +
                    "\n\tQuantity: " + quantity +
                    "\n\tPrice: " + price +
                    "\n\tOrderDate: " + orderDate + "\n";
        }
    }
}
