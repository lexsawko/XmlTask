package SaxParser;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class OrderHandler extends DefaultHandler {
    private List<Order> orderList = new ArrayList<>();
    private Order order;
    private Order.ShippingAndBillTo currentObject;
    private Order.Item currentItem;
    private String elementName= "";


    @Override
    public void startDocument() throws SAXException {
        System.out.println("SAX парсер начал работу");
        System.out.println("------------------------");
    }

    @Override
    public void endDocument() throws SAXException {
        System.out.println("Вывод списка заказов");
        for (Order order:orderList) {
            System.out.println(order.toString());
        }
        System.out.println("SAX парсер закончил работу");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        switch (qName){
            case "order_list": {
                System.out.println("Начало тега order_list");
                break;
            }
            case "order": {
                System.out.println("Начало тега order, создание объекта Order");
                order = new Order();
                break;
            }
            case "shipping_to":{
                currentObject = order.getShippingTo();
                System.out.println("Начало тега shipping_to, запись атрибута");
                currentObject.setCountry(attributes.getValue(0));
                break;
            }
            case "bill_to":{
                currentObject = order.getBillTo();
                System.out.println("Начало тега bill_to, запись атрибута");
                currentObject.setCountry(attributes.getValue(0));
                break;
            }
            case "first_name":{
                elementName = "first_name";
                System.out.println("Начало тега first_name");
                break;
            }
            case "last_name":{
                elementName = "last_name";
                System.out.println("Начало тега last_name");
                break;
            }
            case "street":{
                elementName = "street";
                System.out.println("Начало тега street");
                break;
            }
            case "city":{
                elementName = "city";
                System.out.println("Начало тега city");
                break;
            }
            case "state":{
                elementName = "state";
                System.out.println("Начало тега state");
                break;
            }
            case "zip":{
                elementName = "zip";
                System.out.println("Начало тега zip");
                break;
            }
            case "item_list":{
                System.out.println("Начало тега item_list");
                break;
            }
            case "item":{
                System.out.println("Начало тега item, создание объекта Item");
                currentItem = order.new Item();
                currentItem.setCode(Integer.parseInt(attributes.getValue(0)));
                break;
            }
            case "item_name":{
                elementName="item_name";
                System.out.println("Начало тега item_name");
                break;
            }
            case "quantity":{
                elementName="quantity";
                System.out.println("Начало тега quantity");
                break;
            }
            case "price":{
                elementName="price";
                System.out.println("Начало тега price");
                break;
            }
            case "order_date":{
                elementName="order_date";
                System.out.println("Начало тега order_date");
                break;
            }
            default:
                System.out.println("Другой тег");break;
        }

    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName){
            case "order": {
                System.out.println("Конец тега order");
                orderList.add(order);
                order = null;
                break;
            }
            case "shipping_to":{
                currentObject = null;
                System.out.println("Конец тега shipping_to");
                break;
            }
            case "bill_to":{
                currentObject = null;
                System.out.println("Конец тега bill_to");
                break;
            }
            case "first_name":
            case "last_name":
            case "street":
            case "city":
            case "zip":{
                elementName="";
                break;
            }
            case "item_list":{
                System.out.println("Конец тега item_list");
                break;
            }
            case "item":{
                System.out.println("Конец тега item, запись объекта Item");
                order.getItemList().add(currentItem);
                currentItem=null;
                break;
            }
            case "item_name":
            case "quantity":
            case "price":
            case "order_date":{
                elementName="";
                break;
            }
            default:break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        switch (elementName){
            case "first_name": currentObject.setFirstName(new String(ch,start,length).trim()); break;
            case "last_name": currentObject.setLastName(new String(ch,start,length).trim()); break;
            case "street": currentObject.setStreet(new String(ch,start,length).trim()); break;
            case "city": currentObject.setCity(new String(ch,start,length).trim()); break;
            case "state": currentObject.setState(new String(ch,start,length).trim()); break;
            case "zip": currentObject.setZip(Integer.parseInt(new String(ch,start,length).trim())); break;
            case "item_name": currentItem.setItemName(new String(ch,start,length).trim()); break;
            case "quantity": currentItem.setQuantity(Integer.parseInt(new String(ch,start,length).trim())); break;
            case "price": currentItem.setPrice(Float.parseFloat(new String(ch,start,length).trim())); break;
            case "order_date":
                try {
                    currentItem.setOrderDate(new SimpleDateFormat("yyyy-MM-dd").parse(new String(ch,start,length).trim()));
                } catch (ParseException e) {
                    System.err.println("Date parsing exception" + e);
                }
                break;
            default: break;
        }
    }
}
